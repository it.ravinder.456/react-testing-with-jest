import React from "react";

export const Button = ({ label = "Welcome" }) => {
  return (
    <button data-testid="button" className="btn">
      {label}
    </button>
  );
};
