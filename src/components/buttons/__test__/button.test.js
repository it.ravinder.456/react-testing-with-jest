import React from 'react';
import ReactDOM from 'react-dom';
import { Button } from '../Button';

import { render, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';

// afterEach(cleanup)

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Button label={"click me"} />, div)
})

it("renders button correctly", () => {
    const { getByTestId } = render(<Button label={"click me 2"} />)
    expect(getByTestId('button')).toHaveTextContent("click me 2")
})

it("renders button correctly", () => {
    const { getByTestId } = render(<Button label={"save"} />)
    expect(getByTestId('button')).toHaveTextContent("save")
})


it("compare snapshots", () => {
    const tree = TestRenderer.create(<Button label={"save"} />).toJSON()
    expect(tree).toMatchSnapshot();
})

it("compare snapshots", () => {
    const tree = TestRenderer.create(<Button label={"cancel"} />).toJSON()
    expect(tree).toMatchSnapshot();
})

