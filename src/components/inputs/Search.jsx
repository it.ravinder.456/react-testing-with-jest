import React, { useState } from "react";
import { Button } from "../buttons/Button";

export const Search = ({ showText }) => {
  const [input, setInput] = useState("");
  return (
    <React.Fragment>
      <div className="searchBar">
        <input
          data-testid="searchInput"
          type="text"
          onChange={(e) => setInput(e.target.value)}
        />
      </div>
      {showText && (
        <div className="sumary" data-testid="summary">
          {input}
        </div>
      )}
      <div className="submit">
        <Button />
      </div>
    </React.Fragment>
  );
};
