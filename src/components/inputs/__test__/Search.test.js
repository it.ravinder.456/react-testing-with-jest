import React from 'react';

import { render, cleanup, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect';
import TestRenderer from 'react-test-renderer';
import { Search } from '../Search';

// afterEach(cleanup)

describe("Search Component", () => {


    it("render summary when showText=true", () => {
        const { getByTestId } = render(<Search showText />)
        const summaryText = getByTestId('summary');
        expect(summaryText).toBeTruthy();
    })


    it("dont render summary when showText=false", () => {
        const { queryByTestId } = render(<Search showText={false} />)
        const summaryText = queryByTestId('summary');
        expect(summaryText).toBeFalsy();
    })


    it("changing input causes changes on summary", () => {
        const { getByTestId } = render(<Search showText={true} />)
        const searchInput = getByTestId("searchInput");
        const summaryText = getByTestId('summary');
        const searchTerm = "Ravinder";
        fireEvent.change(searchInput, { target: { value: searchTerm } });
        expect(summaryText.innerHTML).toBe(searchTerm);
    })

})


