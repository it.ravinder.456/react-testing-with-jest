import React from "react";
import { Main } from "./components/Main";
import "./styles/styles.scss";

export const App = () => {
  return <Main />;
};
